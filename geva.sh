#!/bin/bash
# This options tells gridware to change to the current directory before
# executing the job (default is the home of the user)
#$-cwd
#
# This option declares the amount of memory the job will
# use. Specify this value as accurat as possible. If you
# declare too low memory usage, your job may be aborted.
# If you declare too high memory usage, your job might be
# wait for a long time until it is started on a machine
# that has the sufficient amount of free memory.
#$-l vf=1G
#
# Specify this option only for multithreaded jobs that use
# more than one cpu core. The value 1..24 denotes the number
# of requested cpu cores. Please note that multithreaded jobs
# are always calculated on a single machine - for parallel
# jobs you should use MPI instead.
# Another important hint: memory request by -l vf=... are
# multiplied by the number of requested cpu cores. Thus you
# should divide the overall memory consumption of your job by
# the number of parallel threads.
#$-pe serial 5
#
# -- Job name ---
#$ -N geva
#$ -S /bin/bash
#
# Please insert your mail address!
#$-M kai.wei@tum.de

export PERL5LIB=/data/proj/teaching/NGS_course/Softwares/vcftools/src/perl
export PATH=/cluster/gridengine/ge2011.11/bin/linux-x64:/cluster/openmpi/bin:/cluster/java/latest/bin:/cluster/software/bin:/sysinst/bin:/cluster/gridengine/ge2011.11/bin/linux-x64:/usr/lib64/qt-3.3/bin:/cluster/openmpi/bin:/cluster/java/latest/bin:/cluster/software/bin:/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin:/root/bin:/data/proj/teaching/NGS_course/bin:/data/proj/teaching/NGS_course/Softwares/vcftools/src/perl

vcf=/data/proj/chilense/30_genomes_outputs/kai_wei/variant_call/CNVs/popgen/chilense_no_outgroup.vcf

#vcftools --vcf $vcf --chr 1 --out chr1 --recode --recode-INFO-all
#vcftools --vcf $vcf --chr 2 --out chr2 --recode --recode-INFO-all
#vcftools --vcf $vcf --chr 3 --out chr3 --recode --recode-INFO-all
#vcftools --vcf $vcf --chr 4 --out chr4 --recode --recode-INFO-all
#vcftools --vcf $vcf --chr 5 --out chr5 --recode --recode-INFO-all
#vcftools --vcf $vcf --chr 6 --out chr6 --recode --recode-INFO-all
#vcftools --vcf $vcf --chr 7 --out chr7 --recode --recode-INFO-all
#vcftools --vcf $vcf --chr 8 --out chr8 --recode --recode-INFO-all
#vcftools --vcf $vcf --chr 9 --out chr9 --recode --recode-INFO-all
#vcftools --vcf $vcf --chr 10 --out chr10 --recode --recode-INFO-all
#vcftools --vcf $vcf --chr 11 --out chr11 --recode --recode-INFO-all
#vcftools --vcf $vcf --chr 12 --out chr12 --recode --recode-INFO-all

#extract snp in drought gene regions
 vcftools --vcf $vcf --bed turquoise_genes.bed --out turquoise_genes --recode --recode-INFO-all
 vcftools --vcf $vcf --bed blue_genes.bed --out blue_genes --recode --recode-INFO-all

#check allele frequence
vcftools --vcf blue_genes.recode.vcf --freq --out blue
vcftools --vcf turquoise_genes.recode.vcf --freq --out turquoise


# create inputs of geva from vcf for each chroms
 for line in {1..12};do
 mkdir chr$line
 /data/home/users/k.wei/Tools/geva-master/geva_v1beta --vcf chr$line.recode.vcf --rec 3.24e-9 --out chr$line/chr$line
 done


##estimation of allele age
 /data/home/users/k.wei/Tools/geva-master/geva_v1beta -i chr1/chr1.bin -o RUN4 --position 729256 --Ne 20000 --mut 5.1e-9 -t 5 --hmm /data/home/users/k.wei/Tools/geva-master/hmm/hmm_initial_probs.txt /data/home/users/k.wei/Tools/geva-master/hmm/hmm_emission_probs.txt

 Rscript /data/home/users/k.wei/Tools/geva-master/estimate.R RUN4.pairs.txt 20000

